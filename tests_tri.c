/**
 * @file 
 * @author Franck
 * @version 1.1
 * @brief Fichier de fonctions de tris, benchmarks et création CSV
 * @date 23-09-2019
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "triLib.h"

/**
 * @brief Fonction qui renvoi la durée pour trier le tableau avec une fonction de tri a bulle
 * @param tab tableau a tester
 * @param sizeoftab taille du tableau a tester
 * @return durée de traitement du tableau
 */
float bench_triabulle(float *tab, int sizeoftab){
    float *tab1 = malloc(sizeof(float)*sizeoftab); 

    /* copie du tableau dans un nouveau afin de garder les 
     * modifications locales */
    for(int i = 0; i < sizeoftab; i++)
       tab1[i] = tab[i]; 
    
    /* récuperation de la date actuelle */
    float startTime = (float)clock()/CLOCKS_PER_SEC;

    /* tri du tableau */
    triabulle(tab1, sizeoftab);   

    /* récuperation de la date actuelle */
    float endTime = (float)clock()/CLOCKS_PER_SEC;

    /* recupération de la durée d'execution du tri*/
    float timeElapsed = endTime - startTime;
    
    /* renvoi du résultat */
    return timeElapsed;
}

/**
 * Fonction qui renvoi la durée pour trier le tableau avec une fonction de tri par selection
 * @param tab tableau a tester
 * @param sizeoftab taille du tableau a tester
 * @return durée de traitement du tableau
 */
float bench_triparselection(float *tab, int sizeoftab){
    float *tab1 = malloc(sizeof(float)*sizeoftab); 

    /* copie du tableau dans un nouveau afin de garder les 
     * modifications locales */
    for(int i = 0; i < sizeoftab; i++)
       tab1[i] = tab[i]; 
    
    /* récuperation de la date actuelle */
    float startTime = (float)clock()/CLOCKS_PER_SEC;

    /* tri du tableau */
    triparselection(tab1, sizeoftab);   

    /* récuperation de la date actuelle */
    float endTime = (float)clock()/CLOCKS_PER_SEC;

    /* recupération de la durée d'execution du tri*/
    float timeElapsed = endTime - startTime;
    
    /* renvoi du résultat */
    return timeElapsed;
}

/**
 * Fonction qui renvoi la durée pour trier le tableau avec une fonction de tri par insertion
 * @param tab tableau a tester
 * @param sizeoftab taille du tableau a tester
 * @return durée de traitement du tableau
 */
float bench_triparinsertion(float *tab, int sizeoftab){
    float *tab1 = malloc(sizeof(float)*sizeoftab); 

    /* copie du tableau dans un nouveau afin de garder les 
     * modifications locales */
    for(int i = 0; i < sizeoftab; i++)
       tab1[i] = tab[i]; 
    
    /* récuperation de la date actuelle */
    float startTime = (float)clock()/CLOCKS_PER_SEC;

    /* tri du tableau */
    triparinsertion(tab1, sizeoftab);   

    /* récuperation de la date actuelle */
    float endTime = (float)clock()/CLOCKS_PER_SEC;

    /* recupération de la durée d'execution du tri*/
    float timeElapsed = endTime - startTime;
    
    /* renvoi du résultat */
    return timeElapsed;
}

/**
 * Fonction qui crée un CSV contenant le compartif des durées de tri des 
 * différents algorithmes de tri à partir d'un tableau de données.
 * @param filename nom du fichier généré.
 * @param a tableau contenant les données à générer.
 */
void create_csv(char *filename,float a[4][3]){
 
    FILE *fp;

    filename=strcat(filename,".csv");

    fp=fopen(filename,"w+");

    fprintf(fp,",tri a bulle,tri par selection,tri par insertion\n");
    fprintf(fp,"100,%f,%f,%f\n",a[0][0],a[0][1],a[0][2]);
    fprintf(fp,"1000,%f,%f,%f\n",a[1][0],a[1][1],a[1][2]);
    fprintf(fp,"10000,%f,%f,%f\n",a[2][0],a[2][1],a[2][2]);
    fprintf(fp,"moyenne,%f,%f,%f",a[3][0],a[3][1],a[3][2]);

    fclose(fp);

    printf("Fichier %s créé",filename);
 
}