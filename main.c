/**
 * @file 
 * @author Franck et Lois
 * @version 1.1
 * @brief fichier main
 * @date 23-09-2019
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include "triLib.h"

/*! \mainpage Accueil
 *
 * \section intro_sec Synopsis
 *
 * Benchme est une application de benchmarks de fonctions de 
 * tri avec génération des résultats dans un fichier CSV
 *
 * \section install_sec Installation
 *
 * \subsection step1 Etape 1: Ouvrez l'invité de commande
 * \subsection step2 Etape 2: Executez la commande ./benchme <nom du fichier CSV à créer>
 * \subsection step3 Etape 3: Ouvrez le fichier CSV créé
 */


/** @brief Fonction de benchmark qui test 3 tableaux
 * avec 3 fonctions de tris : 
 * - tri a bulles
 * - tri par selection
 * - tri par insertion
 * puis renvoi les valeurs dans une fonction de création de CSV
    @param file nom du fichier
*/
void benchme(char *file){
    int sizeofTab = 100;
    int sizeofTab2 = 1000;
    int sizeofTab3 = 10000;
    float *tab = malloc(sizeof(float)*sizeofTab);
    float *tab2 = malloc(sizeof(float)*sizeofTab2);
    float *tab3 = malloc(sizeof(float)*sizeofTab3); 
    float moy = 0; /* moyenne des resultats */
    float results[4][3]; /* tableau de resultats des benchmarks */
   
    /* randomisation des variables */
    for(int i = 0; i < sizeofTab; i++)
       tab[i]=((float)rand()/(float)(RAND_MAX)) * pow(10,6);
    for(int i = 0; i < sizeofTab2; i++)
       tab2[i]=((float)rand()/(float)(RAND_MAX)) * pow(10,6);
    for(int i = 0; i < sizeofTab3; i++)
       tab3[i]=((float)rand()/(float)(RAND_MAX)) * pow(10,6);
    
    /*trier 3 fois avec tri a bulles
     * envoyer les valeurs dans un tableau
     * calculer la moyenne
     */
    results[0][0] = bench_triabulle(tab,sizeofTab);
    moy += results[0][0];
    
    results[1][0] = bench_triabulle(tab2,sizeofTab2);
    moy += results[1][0];

    results[2][0] = bench_triabulle(tab3,sizeofTab3);
    moy += results[2][0];

    results[3][0] = moy/3;
    
    /*trier 3 fois avec tri par selection
     * envoyer les valeurs dans un tableau
     * calculer la moyenne
     */
    moy = 0;
    results[0][1] = bench_triparselection(tab,sizeofTab);
    moy += results[0][1];
    
    results[1][1] = bench_triparselection(tab2,sizeofTab2);
    moy += results[1][1];
        
    results[2][1] = bench_triparselection(tab3,sizeofTab3);
    moy += results[2][1];
    
    results[3][1] = moy/3;
    
    /*trier 3 fois avec tri par insertion
     * envoyer les valeurs dans un tableau
     * calculer la moyenne
     */
    moy = 0;
    results[0][2] = bench_triparinsertion(tab,sizeofTab);
    moy += results[0][2];
    
    results[1][2] = bench_triparinsertion(tab2,sizeofTab2);
    moy += results[1][2];
        
    results[2][2] = bench_triparinsertion(tab3,sizeofTab3);
    moy += results[2][2];
    
    results[3][2] = moy/3;
    
    /* creation du fichier CSV */
    create_csv(file,results);
}

/**
 * Fonction principale qui execute la fonction de benchmarks
 * @param argc nombre d'arguments
 * @param argv[1] nom du fichier à générer
 * @return erreur si il y a
 */
int main(int argc, char** argv) { //fonction principale 
    
    /* initialisation du seed pour la génération de valeurs aléatoires du programme */
    srand(time(0));
    
    benchme(argv[1]);
        
    return (EXIT_SUCCESS);
}