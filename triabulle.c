/**
 * @file 
 * @author Lois
 * @version 1.1
 * @brief fichier de la fonction de tri a bulle
 * @date 25-09-2019
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * @brief Tri d'un tableau avec une fonction de tri a bulle
 * @param tab tableau a trier
 * @param sizeoftab taille du tableau a trier
 */
void triabulle(float *tab, int sizeoftab){
    float _temp; /*déclaration variable temporaire */
    int changed = 1; /*déclaration booléen pour while*/
    
    while(changed == 1){ /*boucle while qui tourne tant que le tableau n'est pas trié*/
        changed = 0;
        for(int j = 0; j < sizeoftab-1; j++){ /*on parcourt le tableau */
       
            if(tab[j] > tab[j+1]){ /*si le nombre d'avant est > au suivant*/
                _temp = tab[j];
                tab[j] = tab[j+1]; /*on swap les 2 valeurs à l'aide de _temp*/
                tab[j+1] = _temp;

                changed = 1; /*quand le tableau est trié, le booléen passe à 1*/
            }   
       }   
    }
}


