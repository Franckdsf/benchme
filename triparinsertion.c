/**
 * @file 
 * @author Lois
 * @version 1.1
 * @brief fichier de la fonction de tri par insertion
 * @date 25-09-2019
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * @brief Tri d'un tableau avec une fonction de tri par insertion
 * @param tab tableau a trier
 * @param sizeoftab taille du tableau a trier
 */
void triparinsertion(float *tab, int n)
{
    int i, j;           /*on déclare les variables*/
    float tabvalue;

    for (i = 1; i < n; i++) {
        tabvalue = tab[i]; /*pour chaque nb genéré aléatoirement, on l'entre dans le tab*/
        
        /* Décale les éléments situés avant tab[i] vers la droite
           jusqu'à trouver la position d'insertion */
        for (j = i; j > 0 && tab[j - 1] > tabvalue; j--) {
            tab[j] = tab[j - 1];
        }
        /*insertion de la valeur stockée tab[j] à la place vacante*/
        tab[j] = tabvalue;
    }
}

